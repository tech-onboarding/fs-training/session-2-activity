import {Row, Col, Button} from "react-bootstrap";

/*Pass-by-value (parameter passing)
	let name = "John Doe"
	function fullname(myName) {
	console.log(`Hello, I am ${}`)
	}

	fullName(name);

	Pass-by-reference(object, array)
	
*/

export default function Banner({data}) {
	
	const {title, content, label} = data;

	return (
		<Row>
		    <Col className="p-5 m-5 text-center">
		        <h1>{title}</h1>
		        <p>{content}</p>
		        <Button>{label}</Button>
		    </Col>
		</Row>
	)
}