import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*
  JSX - JAvascript + XML
    - is an extension of JS that lets us create objects which then be compiled and added as HTML elements
    - we are able to create HTML elements using JS
    without using the following methods
    .value - forms
    .innerHTML - div, p, span

  JSX provides "syntactic sugar" for creating react components.
  syntactic sugar - programming language that is designed to make things easier to read and or express. 
  <Home />
*/


// const name = "Daisyrie Dela Cruz"
// const element = <h1>Hello, {name}</h1>

// root.render(element);